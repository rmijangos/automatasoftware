﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MODELS
{
    public class LineTansitionFunction
    {
        public string sourceState { get; set; }
        public string alphabetEdge { get; set; }
        public string targetState { get; set; }
    }
}
