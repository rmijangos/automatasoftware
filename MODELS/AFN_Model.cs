﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Diagnostics;

namespace MODELS
{
    public class AFN_Model
    {
        public List<string> States { get; set; }                //Q.    States of AFN
        public List<string> Alphabet { get; set; }              //F.    Alphabet of AFN with epsilon
        public string InitialState { get; set; }                //i.    Initial state AFN
        public List<string> AcceptanceStates { get; set; }      //A.    Acceptance states AFN
        public string[] TranAFN{ get; set; }                    //W.    Contains all the transitions from TXT separated by comma 
        //############################################################################################################################
        public string[,] TranAFD { get; set; }                  //Rev
        public string[] TitlesMatrix { get; set; }
        public List<string> AlphabetTOAFD { get; set; }
        public Stack ListOfStatesToAFD { get; set; }
        public List<string> StatesWithoutFlag { get; set; }
        public List<string> AddedStatesAFD { get; set; }
        public List<string> StatesAFD { get; set; }             //Q.    States of AFD
        public string T { get; set; }
        public string TWhenThereIsMoreThanOneNewState { get; set; }
        public string TransitionFunctionMove { get; set; }      //Usado como parametro en la función cerradura_e
        public List<string> transitionsAFD { get; set; }
        public string initialStateAFD { get; set; }
        public List<string> AcceptanceStatesAFD { get; set; }


        public AFN_Model(List<string> states, List<string> alphabet, string initialState, List<String> acceptanceStates, string[] tranAFN, string[,] tranAFD, string[] titlesMatrix, List<string> alphabetTOAFD, Stack listOfStatesToAFD) {
            this.States = states;
            this.Alphabet = alphabet;
            this.InitialState = initialState;
            this.AcceptanceStates = acceptanceStates;
            this.TranAFN = tranAFN;
            this.Alphabet.Add("e");
            this.TranAFD = tranAFD;
            this.TitlesMatrix = titlesMatrix;
            this.AlphabetTOAFD = alphabetTOAFD;
            this.ListOfStatesToAFD = listOfStatesToAFD;
            this.StatesWithoutFlag = new List<string>();
            this.AddedStatesAFD = new List<string>();
            this.StatesAFD = new List<string>();
            this.T = string.Empty;
            this.TransitionFunctionMove = string.Empty;
            this.TWhenThereIsMoreThanOneNewState = string.Empty;
            this.transitionsAFD = new List<string>();
            this.initialStateAFD = string.Empty;
            this.AcceptanceStatesAFD = new List<string>();
        }

    }
}
