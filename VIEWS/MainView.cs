﻿#region NAMESPACES
using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using CONTROLLERS;
using MODELS;
#endregion

namespace TEST_AUTOMATA
{
    public partial class MainView : Form
    {
        AFN_Model AFN_AFD_obj;
        #region MAIN_VIEW
        public MainView()
        {
            InitializeComponent();
        }
        #endregion

        private void button1_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                lblPathFIle.Text = openFileDialog1.FileName;
                //*************************************************** Adding info to list view - Content File    *******************************************
                #region CONTENT_FILE
                listViewContentFile.Items.Clear();
                listViewContentFile.View = View.Details;
                listViewContentFile.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
                listViewContentFile.Scrollable = true;
                var lst = ControlMainView.ReadFileLineByLineContentFIle(openFileDialog1.FileName);
                foreach (var item in lst){ listViewContentFile.Items.Add(item); }
                #endregion
                //*************************************************** Adding info to list view - Automata States (Q)    ************************************
                #region AUTOMATA_STATES
                listViewStates_Q.Items.Clear();
                listViewStates_Q.View = View.Details;
                listViewStates_Q.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
                listViewStates_Q.Scrollable = true;
                var lstQ = ControlMainView.GenerateSetsAutomata(openFileDialog1.FileName, 'Q');
                foreach (var item in lstQ){ listViewStates_Q.Items.Add(item); }
                #endregion
                //*************************************************** Adding info to list view - Automata Alphabet (sigma)    ******************************
                #region AUTOMATA_ALPHABET
                listViewAlphabet_Sigma.Items.Clear();
                listViewAlphabet_Sigma.View = View.Details;
                listViewAlphabet_Sigma.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
                listViewAlphabet_Sigma.Scrollable = true;
                var lstSigma = ControlMainView.GenerateSetsAutomata(openFileDialog1.FileName, 'F');
                foreach (var item in lstSigma){ listViewAlphabet_Sigma.Items.Add(item); }
                #endregion
                //*************************************************** Adding info to list view - Automata Acceptance States    *****************************
                #region AUTOMATA_ACEPTANCE_STATES
                listViewAcceptanceStates.Items.Clear();
                listViewAcceptanceStates.View = View.Details;
                listViewAcceptanceStates.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
                listViewAcceptanceStates.Scrollable = true;
                var lstAcceptanceStates = ControlMainView.GenerateSetsAutomata(openFileDialog1.FileName, 'A');
                foreach (var item in lstAcceptanceStates){ listViewAcceptanceStates.Items.Add(item); }
                #endregion
                //***************************************************    Building transition function    ***************************************************
                #region TRANSITION_FUNCTION
                List<string> lststates = new List<string>();
                List<string> lstAlphabet = new List<string>();
                lststates.Clear();
                lstAlphabet.Clear();
                dgvTransitionFunction.Columns.Clear();
                dgvTransitionFunction.Rows.Clear();
                dgvTransitionFunction.Refresh();
                string[] states = ControlMainView.TranstionFunction(openFileDialog1.FileName);
                //Adding column of states
                dgvTransitionFunction.Columns.Add("States", "States");
                foreach (var item in states)
                {
                    string[] splitLineState = item.Split(',');
                    //First evaluate and addig row state
                    if (lststates.Exists(x => x == splitLineState[0].ToString()) == false) { addRow(splitLineState[0]); lststates.Add(splitLineState[0]); }
                    //Second evaluate and addig headers alphabet
                    if (lstAlphabet.Exists(x => x == splitLineState[1].ToString()) == false) { createColumn(splitLineState[1]); lstAlphabet.Add(splitLineState[1]); }
                    //Third adding target row to column of alphatet state
                    int _ixSourceState = lststates.IndexOf(splitLineState[0]);
                    int _ixCellAlphabet = lstAlphabet.IndexOf(splitLineState[1])+1;
                    updateRowTargetState(splitLineState[2], _ixSourceState, _ixCellAlphabet);
                }
                #endregion
                //***************************************************        Automata initial state      ***************************************************
                #region AUTOMATA_INITIAL_STATE_IS_DISABLED
                //listViewAcceptanceStates.Clear();
                //Adding info to list view - Automata Initial State
                //listViewInitialState.View = View.Details;
                //listViewInitialState.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
                //listViewInitialState.Scrollable = true;
                string initialState = ControlMainView.InitialState(openFileDialog1.FileName, 'i');
                //Debug.WriteLine("The initial state is: "+ initialState);

                //listViewInitialState.Items.Add(lstInitialState);
                #endregion

                #region AFN - AFD CONVERSION
                List<string> alphabetTOAFD = new List<string>(lstSigma);
                string[,] tranAFD = new string[500, lstSigma.Count+3];
                string[] titles = DefiningTitlesMatrixAFD(lstSigma);
                Stack listOfStates = new Stack();
                listOfStates = ListOfStates();
                AFN_Model AFN = new AFN_Model(lstQ, lstSigma, initialState, lstAcceptanceStates, states, tranAFD, titles, alphabetTOAFD, listOfStates);
                BuildFunctionTransitionAFDGUI(AFN_AFD.AfnConversion(AFN));
                


                //dgvTransitionFunctionAFD;
                #endregion
            }
        }
        #region ANOTHER_METHODS
        private void createColumn(string nameOfColumn) {
            dgvTransitionFunction.Columns.Add(nameOfColumn, nameOfColumn);
        }

        private void addRow(string data) {
            int ixRow = dgvTransitionFunction.Rows.Add();
            dgvTransitionFunction.Rows[ixRow].Cells[0].Value = data;
            dgvTransitionFunction.AutoResizeColumn(0);
        }

        private void updateRowTargetState(string data, int ixSourceState ,int ixCellAlphabet) {
            dgvTransitionFunction.Rows[ixSourceState].Cells[ixCellAlphabet].Value = data;
        
        }

        private Stack ListOfStates() {
            Stack listOfStates = new Stack();

            //From AA-ZZ
            for (int i = 25; i >= 0; i--)
            {
                StringBuilder sb = new StringBuilder();
                char val = Convert.ToChar(i + 65);
                sb.Append(val);
                sb.Append(val);
                listOfStates.Push(sb.ToString());
            }

            //From A-Z
            for (int i = 25; i >= 0; i--)
            {
                StringBuilder sb = new StringBuilder();
                char val = Convert.ToChar(i + 65);
                sb.Append(val);
                listOfStates.Push(sb.ToString());
            }

            return listOfStates;
        }

        private string[] DefiningTitlesMatrixAFD(List<string> lstSigma) {
            StringBuilder _titles = new StringBuilder("Flag,Estados,");
            foreach (var item in lstSigma)
            {
                _titles.Append(item + ",");
            }
            _titles.Append("Composicion");
            //Debug.WriteLine(_titles.ToString());
            string[] titles = _titles.ToString().Split(',');

            return titles;
        }

        private void BuildFunctionTransitionAFDGUI(AFN_Model obj) {
            BuildAFNContentTXT(obj);
            dgvTransitionFunctionAFD.Columns.Clear();
            dgvTransitionFunctionAFD.Rows.Clear();
            dgvTransitionFunctionAFD.Refresh();

            for (int i = 0; i < obj.StatesAFD.Count + 1; i++)
            {
                for (int j = 0; j < obj.Alphabet.Count + 2; j++)
                {
                    if (i == 0) {
                        dgvTransitionFunctionAFD.Columns.Add(obj.TranAFD[i, j], obj.TranAFD[i, j]);
                        //dgvTransitionFunctionAFD.Columns[i].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                    } 
                    if (i != 0) {
                        if(j == 0)dgvTransitionFunctionAFD.Rows.Add();
                        dgvTransitionFunctionAFD.Rows[i-1].Cells[j].Value = obj.TranAFD[i, j];
                    }
                }
            }
        }

        private void BuildAFNContentTXT(AFN_Model obj) {
            //listViewContentFileAFD
            StringBuilder ContentAFNTXT = new StringBuilder();
            listViewContentFileAFD.Items.Clear();
            listViewContentFileAFD.View = View.Details;

            int ix = 0;
            while (ix < obj.StatesAFD.Count)
            {
                if(ix == 0) ContentAFNTXT.Append("Q:{"+obj.TranAFD[ix + 1, 1] + ",");
                else if (ix == obj.StatesAFD.Count - 1) ContentAFNTXT.Append(obj.TranAFD[ix + 1, 1]+"}");
                else ContentAFNTXT.Append(obj.TranAFD[ix + 1, 1] + ",");
                ix++;
            }
            listViewContentFileAFD.Items.Add(ContentAFNTXT.ToString());
            ContentAFNTXT.Clear();
            ix = 0;
            while (ix < obj.AlphabetTOAFD.Count)
            {
                if (ix == 0) ContentAFNTXT.Append("F:{" + obj.AlphabetTOAFD[ix] + ",");
                else if (ix == obj.AlphabetTOAFD.Count -1) ContentAFNTXT.Append(obj.AlphabetTOAFD[ix] + "}\n");
                else ContentAFNTXT.Append(obj.AlphabetTOAFD[ix] + ",");
                ix++;
            }
            listViewContentFileAFD.Items.Add(ContentAFNTXT.ToString());
            listViewContentFileAFD.Items.Add("i:"+obj.TranAFD[1,1]);
            obj.initialStateAFD = obj.TranAFD[1, 1];

            ContentAFNTXT.Clear();
            int counterAcceptanceState = 0;
            foreach (var item in obj.StatesAFD)
            {
                foreach (var acceptState in obj.AcceptanceStates)
                {
                    //AcceptanceStates
                    string[] stateInfo = item.Split(',');
                    string[] transition = stateInfo[2].Replace("_:_:_", ",").Split(',');

                    foreach (var st in transition)
                    {
                        if (st.ToString() == acceptState){ 
                            counterAcceptanceState++;
                        }
                    }
                    if (counterAcceptanceState == obj.AcceptanceStates.Count)
                    {
                        ContentAFNTXT.Append(stateInfo[0] + ",");
                        obj.AcceptanceStatesAFD.Add(stateInfo[0]);
                        counterAcceptanceState = 0;
                    }
                }
            }
            listViewContentFileAFD.Items.Add("A:{" + ContentAFNTXT.ToString().Remove(ContentAFNTXT.ToString().Length - 1) + "}");

            ContentAFNTXT.Clear();
            obj.transitionsAFD.Clear();
            int ix_row = 2;
            foreach (var elementAlphabet in obj.AlphabetTOAFD)
            {
                foreach (var state in obj.StatesAFD)
                {
                    string[] splitState = state.Split(',');
                    obj.transitionsAFD.Add(splitState[0] + "," + elementAlphabet + "," + obj.TranAFD[Convert.ToInt32(splitState[1]), ix_row]);
                    ContentAFNTXT.Append("("+splitState[0]+","+ elementAlphabet + "," + obj.TranAFD[Convert.ToInt32(splitState[1]),ix_row]+");");
                }
                ix_row++;
            }
            listViewContentFileAFD.Items.Add("w:{"+ContentAFNTXT.ToString().Remove(ContentAFNTXT.Length-1) +"}");
            AFN_AFD_obj = obj;
        }


        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void groupBox4_Enter(object sender, EventArgs e)
        {

        }

        private void groupBox3_Enter(object sender, EventArgs e)
        {

        }

        private void groupBox4_Enter_1(object sender, EventArgs e)
        {

        }
        #endregion

        private void dgvTransitionFunction_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void groupBox7_Enter(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }


        #region STRING EVALUATION
        private void txtStringToEvaluate_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                string stateResponse = AFN_AFD_obj.initialStateAFD;

                //txtStringToEvaluate.Text.ToString()
                foreach (var item in txtStringToEvaluate.Text.ToString())
                {
                    stateResponse = MoveElement(stateResponse, item.ToString());
                }

                int val =  AFN_AFD_obj.AcceptanceStatesAFD.IndexOf(stateResponse);

                Console.WriteLine("Find ix " + val);
                Console.WriteLine("Final state response: "+ stateResponse);

                if (val != -1) lblResponseStringEvaluate.Text = "Acceptance chain"; else lblResponseStringEvaluate.Text = "Chain of non-acceptance";

            }
        }

        private string MoveElement(string state, string inputElement) {
            string nextState = string.Empty;
            foreach (var transition in AFN_AFD_obj.transitionsAFD)
            {
                string[] splitTransition = transition.Split(',');
                if (splitTransition[0].Equals(state) && splitTransition[1].Equals(inputElement))
                {
                    nextState = splitTransition[2].ToString();
                }
            }
            return nextState;
        }
        #endregion

        private void tableLayoutPanel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void groupBox8_Enter(object sender, EventArgs e)
        {

        }

        private void txtStringToEvaluate_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
