﻿#region NAMESPACES
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using MODELS;
using System.Diagnostics;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.Generic;
#endregion

namespace CONTROLLERS
{
    public static class ControlMainView
    {
        public static List<string> ReadFileLineByLineContentFIle(string path)
        {
            List<string> listContentRawFile = new List<string>();
            using (StreamReader sr = File.OpenText(path))
            {
                string lineFile;
                while ((lineFile = sr.ReadLine()) != null){ listContentRawFile.Add(lineFile); }
            }
            return listContentRawFile;
        }

        public static List<string> GenerateSetsAutomata(string path, char set)
        {
            List<char> listResultReadingFile = new List<char>();
            List<string> listSetAutomata = new List<string>();
            listResultReadingFile.Clear();
            listSetAutomata.Clear();

            using (StreamReader fileRead = File.OpenText(path))
            {
                while (!fileRead.EndOfStream)
                {
                    int s = fileRead.Read();
                    listResultReadingFile.Add(Convert.ToChar(s));
                }
            }

            //Method to create a list of sets of Q, F, A
            for (int i = 0; i < listResultReadingFile.Count; i++)
            {
                int endingSearch = 0;

                if (listResultReadingFile[i] == set && listResultReadingFile[i + 1] == ':')
                {
                    int b = i;
                    for (int a = b; a < listResultReadingFile.Count; a++)
                    {
                        if (listResultReadingFile[a] == ':')
                        {
                            int d = a;
                            for (int c = d; c < listResultReadingFile.Count; c++)
                            {
                                if (listResultReadingFile[c] == '{')
                                {
                                    /******/
                                    int e = c;
                                    string newWord = string.Empty;
                                    for (int j = e + 1; j < listResultReadingFile.Count; j++)
                                    {
                                        if (listResultReadingFile[j] != '{' && listResultReadingFile[j] != '}')
                                        {
                                            if (listResultReadingFile[j] != ',' && listResultReadingFile[j] != '}')
                                            {
                                                newWord += listResultReadingFile[j];
                                            }
                                            else
                                            {
                                                listSetAutomata.Add(newWord);
                                                newWord = string.Empty;
                                            }
                                        }
                                        else if (listResultReadingFile[j] == '}') { listSetAutomata.Add(newWord); endingSearch = 1; break; }
                                    }
                                    /******/
                                }
                                if (endingSearch == 1) { break; }
                            }
                        }
                        if (endingSearch == 1) { break; }
                    }
                }
                if (endingSearch == 1) { break; }
            }

            return listSetAutomata;
        }

        public static string InitialState(string path, char set)
        {
            List<char> listResultReadingFile = new List<char>();
            int secondPart = 0;
            int ending = 0;
            string initialState = string.Empty;


            using (StreamReader fileRead = File.OpenText(path))
            {
                //string s;
                while (!fileRead.EndOfStream)
                {
                    int s = fileRead.Read();
                    listResultReadingFile.Add(Convert.ToChar(s));
                }

            }


            for (int i = 0; i < listResultReadingFile.Count; i++)
            {
                if (listResultReadingFile[i] == set)
                {
                    int j = i;
                    for (int x = j; x < listResultReadingFile.Count; x++)
                    {
                        if (listResultReadingFile[x] == ':')
                        {
                            int k = x;
                            //initialState = String.Empty;
                            for (int y = k; y < listResultReadingFile.Count; y++)
                            {
                                /***********/
                                if (Convert.ToInt32(listResultReadingFile[y]) != 32 && secondPart == 0 && listResultReadingFile[y] != ':')
                                {
                                    initialState += listResultReadingFile[y];
                                    secondPart = 1;
                                }
                                else if (secondPart == 1 && listResultReadingFile[y] != ':' && Convert.ToInt32(listResultReadingFile[y]) != 13 && Convert.ToInt32(listResultReadingFile[y]) != 10)
                                {
                                    initialState += listResultReadingFile[y];
                                }
                                else if (Convert.ToInt32(listResultReadingFile[y]) == 10 || Convert.ToInt32(listResultReadingFile[y]) == 13)
                                {
                                    ending = 1;
                                    secondPart = 0;
                                    break;
                                }
                                /***********/
                            }
                        }
                        if (ending == 1) { break; }
                    }
                }
            }

            return initialState;
        }

        public static string[] TranstionFunction(string path) {
            StringBuilder lineRegex = new StringBuilder();
            using (StreamReader fileRead = File.OpenText(path))
            {
                while (!fileRead.EndOfStream) {lineRegex.Append(fileRead.ReadLine());}
            }
            //string Text = "Q:{A,B1,C,D,F,G,H}W:{(A,a,D);(A,b,B);(B,a,D);(B,b,C);(C,a,C);(C,b,C);(D,a,D);(D,b,D)}F:{1,2,3,@,10}";
            var regex = new Regex(@"(W|w:{)(.*?)(})");
            var match = regex.Match(lineRegex.ToString());
            string ocurrenceGroup = Convert.ToString(match.Groups[2]);
            StringBuilder sb = new StringBuilder(ocurrenceGroup);
            sb.Replace("(", "");
            sb.Replace(")", "");
            string[] data = Convert.ToString(sb).Split(';');

            //Creating data structure Matrix AFN
            //foreach (var item in data) {
            //    Debug.WriteLine(item);
            //}

            return data;
        }
    }
}
