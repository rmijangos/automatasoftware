﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Collections;
using MODELS;


namespace CONTROLLERS
{
    public class AFN_AFD
    {
        public static AFN_Model AfnConversion(AFN_Model obj) {
            BuildMatrixAFD(obj, true);
            AddNew_T(obj, Cerradura_e(obj, true), true);

            while (obj.StatesWithoutFlag.Count > 0)
            {
                MarkWithFlag_T(obj, obj.StatesWithoutFlag.FirstOrDefault());   
                foreach (var inputSymbol in obj.AlphabetTOAFD)
                {
                    string U = Cerradura_e(obj, false, Move(obj, GetCompositionState(obj), inputSymbol));
                    if (!ExistStateBeforeAddingTranD(obj, U)) AddNew_T(obj, U, false);
                    Tran_D(obj, GetStateAFDSinceComposition(obj, U), inputSymbol);
                }
            }

            //for (int i = 0; i < obj.StatesAFD.Count + 1; i++)
            //{
            //    for (int j = 0; j < obj.Alphabet.Count + 2; j++)
            //    {
            //        //if (obj.TranAFD[i, j] == null) obj.TranAFD[i, j] = "N/A";
            //        Console.Write(obj.TranAFD[i, j] + "\t");
            //    }
            //    Console.WriteLine();
            //}
            return obj;
        }

        private static void BuildMatrixAFD(AFN_Model obj, bool initMatrix)
        {
            if (initMatrix)
            {
                for (int rows = 0; rows < obj.States.Count + 1; rows++)
                {
                    for (int columns = 0; columns < obj.Alphabet.Count + 2; columns++)
                    {
                        if (rows == 0) obj.TranAFD[rows, columns] = obj.TitlesMatrix[columns];
                        else obj.TranAFD[rows, columns] = null;
                    }
                }
            }

            //for (int i = 0; i < obj.States.Count + 1; i++)
            //{
            //    for (int j = 0; j < obj.Alphabet.Count + 2; j++)
            //    {
            //        if (obj.TranAFD[i, j] == null) obj.TranAFD[i, j] = "N/A";
            //        Console.Write(obj.TranAFD[i, j] + "\t");
            //    }
            //    Console.WriteLine();
            //}

        }

        #region Cerradura_e
        private static string Cerradura_e(AFN_Model obj, bool isInitialState, string compositionFromMethodMove = null) {
            #region local variables
            StringBuilder T = new StringBuilder();
            StringBuilder T_temp = new StringBuilder();
            Stack statesWithEpsilon = new Stack();
            string finallyT = string.Empty;
            int counterInitState = 0;
            #endregion
            //Tengo que evaluar que pasa cuando el estado inicial es conjunto vacio
            #region initial state
            if (isInitialState)
            {
                T.Clear();
                T.Append(obj.InitialState + ",");
                //string initialState = obj.InitialState;

                foreach (var transition in obj.TranAFN)
                {
                    string[] splitedTransitions = transition.Split(',');
                    if (splitedTransitions[0].Equals(obj.InitialState) && splitedTransitions[1].Equals("e"))
                    {
                        T.Append(splitedTransitions[2] + ",");
                        //Debug.WriteLine("The init value is = "+T);
                        statesWithEpsilon.Push(splitedTransitions[2]);
                        counterInitState++;
                    }
                }
                if (counterInitState > 0)
                {
                    T = RecursiveStatCerra_e(statesWithEpsilon, T, obj);
                    finallyT = T.ToString().Remove(T.ToString().Length - 1);
                }
                else {
                    //if is empty set then append initial state
                    finallyT = obj.InitialState; //"0,"+
                }

            }
            #endregion
            //Generación de nuevos estados cuando no es un estado inicial
            #region not initial state
            if (!isInitialState)
            {
                if (compositionFromMethodMove == "0") finallyT = compositionFromMethodMove;
                else {
                    foreach (var temporaryState in compositionFromMethodMove.Split(','))    //Esto es para recorrer cada estado del AFN que conforma la composición
                    {
                        foreach (var transition in obj.TranAFN) //Recorro la función de trancisión del AFN
                        {
                            string[] splitedTransitions = transition.Split(','); //Parto la transición en un array 
                            if (splitedTransitions[0].Equals(temporaryState) && splitedTransitions[1].Equals("e")) // Si la transición alcanza un elemento epsilon entonces apilar
                            {
                                T.Append(splitedTransitions[2] + ",");          //Formo el estado
                                statesWithEpsilon.Push(splitedTransitions[2]);  //Apilo el nuevo estado
                            }
                        }
                        T = RecursiveStatCerra_e(statesWithEpsilon, T, obj);
                    }
                    T_temp.Append(JoinCerraeMove(compositionFromMethodMove, T.ToString()));
                    T.Clear();
                    T.Append(T_temp);
                    finallyT = T.ToString().Remove(T.ToString().Length - 1);
                }
            }
            #endregion
            return finallyT;
        }

        private static StringBuilder RecursiveStatCerra_e(Stack statesWithEpsilon, StringBuilder T, AFN_Model obj) {
            #region local variables
            Stack temporaryStatesWithEpsilon = new Stack();
            StringBuilder _T = new StringBuilder(T.ToString());
            #endregion
            if (statesWithEpsilon.Count == 0) return _T;    // Si no hay estados nuevos por analizar, entonces devuelve la composición encontrada

            foreach (string state in statesWithEpsilon) //Recorrer la pila para verificar si un estado alcanza a epsilon
            {
                foreach (var transition in obj.TranAFN) //Recorro la función de trancisión del AFN
                {
                    string[] splitTransition = transition.Split(','); //Parto la transición en un array 
                    if (splitTransition[0] == state && splitTransition[1] == "e")
                    {
                        _T.Append(splitTransition[2] + ",");
                        temporaryStatesWithEpsilon.Push(splitTransition[2]);
                    }
                }
            }
            statesWithEpsilon.Clear(); //Limpio la pila que recibo
            RecursiveStatCerra_e(temporaryStatesWithEpsilon, _T, obj); //Recursividad para buscar mas estados
            return _T;
        }
        #endregion

        //Añade un nuevo valor a la tabla de transición
        private static void AddNew_T(AFN_Model obj, string composition, bool isTheFirsState) {
            //Debug.WriteLine("Number of elements: "+ obj.AlphabetTOAFD.Count); //Alphabet
            //Debug.WriteLine("Number of elements a: " + obj.Alphabet.Count);
            //foreach (var item in obj.AlphabetTOAFD){ Debug.WriteLine("Hello: "+item); }
            if (isTheFirsState)
            {
                obj.TranAFD[1, 1] = obj.ListOfStatesToAFD.Pop().ToString();
                obj.StatesAFD.Add(obj.TranAFD[1, 1] + ",1" + "," + composition.Replace(",", "_:_:_"));   // Función de transición que sirve para rectificar validaciones
                obj.TranAFD[1, obj.AlphabetTOAFD.Count + 2] = composition;                          // Añade composición a la matriz
                obj.StatesWithoutFlag.Add(obj.TranAFD[1, 1]);                                       // Añadimos el estado como un estado no marcado
            }
            else {
                //Primero necesito saber en que estado estoy exactamente y saber el indice al cual me enfrento, despues sumar 1 posición al indice
                //obj.T 
                int ix = GetIndexState(obj, GetLastElementOfStatesAFD(obj)) + 1;
                obj.TranAFD[ix, 1] = obj.ListOfStatesToAFD.Pop().ToString();
                obj.StatesAFD.Add(obj.TranAFD[ix, 1] + "," + (ix) + "," + composition.Replace(",", "_:_:_"));
                obj.TranAFD[ix, obj.AlphabetTOAFD.Count + 2] = composition;
                obj.StatesWithoutFlag.Add(obj.TranAFD[ix, 1]);
            }
        }

        //Marca estados
        private static void MarkWithFlag_T(AFN_Model obj, string state) {
            //Get ix row of state
            obj.T = state; //Current state where we are working
            //Debug.WriteLine("The state received is: " + state);
            //Debug.WriteLine("The ix of state " + state + "is : " + GetIndexState(obj, state));
            obj.TranAFD[GetIndexState(obj, state), 0] = "*";
            obj.StatesWithoutFlag.Remove(state);

        }

        //Obtiene el indice de un estado
        private static int GetIndexState(AFN_Model obj, string state) {
            int ix_state = 0;
            foreach (var item in obj.StatesAFD)
            {
                string[] tSplit = item.Split(',');
                if (tSplit[0].Equals(state)) ix_state = Convert.ToInt32(tSplit[1]);

            }
            return ix_state;
        }

        //Obtiene la composición del estado 
        private static string GetCompositionState(AFN_Model obj) {
            string composition = string.Empty;
            foreach (var item in obj.StatesAFD)
            {
                string[] tSplit = item.Split(',');
                if (tSplit[0].Equals(obj.T)) composition = tSplit[2];

            }
            return composition.Replace("_:_:_", ",");
        }

        private static string GetLastElementOfStatesAFD(AFN_Model obj) {
            string[] state = obj.StatesAFD[obj.StatesAFD.Count - 1].Split(',');

            return state[0];
        }

        private static string GetStateAFDSinceComposition(AFN_Model obj, string U) {
            string StateU = string.Empty;
            foreach (var item in obj.StatesAFD)
            {
                string[] splitState = item.Split(',');
                if (splitState[2].Replace("_:_:_", ",") == U) StateU = splitState[0];
            }
            return StateU;
        }

        private static string Move(AFN_Model obj, string compositionState, string elementOfAlphabet) {
            #region local variables
            StringBuilder transitionWithElementOfAlphabet = new StringBuilder();
            string transition = string.Empty;
            int counterOfTransitions = 0;
            #endregion

            //Debug.WriteLine("Move state T received: " + compositionState);
            string[] T = compositionState.Split(',');

            foreach (var splitComposition in T)
            {
                foreach (var transitionAFN in obj.TranAFN)
                {
                    string[] splitTransitionAFN = transitionAFN.Split(',');
                    if (splitTransitionAFN[0] == splitComposition && splitTransitionAFN[1] == elementOfAlphabet)
                    {
                        transitionWithElementOfAlphabet.Append(splitTransitionAFN[2] + ",");
                        counterOfTransitions++;
                    }
                }
            }

            if (counterOfTransitions == 0) transition = transitionWithElementOfAlphabet.Append("0").ToString();
            if (counterOfTransitions > 0) transition = transitionWithElementOfAlphabet.ToString().Remove(transitionWithElementOfAlphabet.ToString().Length-1);

            //obj.TransitionFunctionMove = transition;
            return transition;
        }

        private static string JoinCerraeMove(string compositionMove, string compositionCerrae ) {
            HashSet<string> compositionMove_Hash = new HashSet<string>();
            HashSet<string> compositionCerrae_Hash = new HashSet<string>();
            StringBuilder newStateT = new StringBuilder();
            List<int> sortStates = new List<int>();
            
            foreach (var item in compositionMove.Split(',')) { compositionMove_Hash.Add(item); }
            foreach (var item in compositionCerrae.Split(',')) { compositionCerrae_Hash.Add(item); }
            
            HashSet<string> T = new HashSet<string>(compositionMove_Hash);
            T.UnionWith(compositionCerrae_Hash);
            //Console.WriteLine(Convert.ToInt32("1"));

            foreach (var item in T) { if(item.Length > 0)sortStates.Add(Convert.ToInt32(item)); }
            //sortStates.OrderBy(x => x);
            foreach (var item in sortStates.OrderBy(x => x)) { newStateT.Append(item+","); }
            newStateT.Append(",");
            return newStateT.ToString().Remove(newStateT.Length-1);
        }

        private static bool ExistStateBeforeAddingTranD(AFN_Model obj, string composition) {
            bool response = false;
            foreach (var item in obj.StatesAFD)
            {
                string[] splitTransition = item.Split(',');
                //Console.WriteLine("Splited transition: " + splitTransition[2].Replace("_:_:_", ","));
                if (splitTransition[2].Replace("_:_:_", ",").Equals(composition)) { response = true; break; } else response = false;
            }
            return response;
        }

        private static void Tran_D(AFN_Model obj, string T, string elemetOfAlphabet) {
            obj.TranAFD[GetIndexState(obj, obj.T), obj.AlphabetTOAFD.IndexOf(elemetOfAlphabet) + 2] = T;
        
        }
    }
}
